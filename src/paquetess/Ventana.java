/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquetess;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import javax.swing.ButtonGroup;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 *
 * @author juanj
 */
public class Ventana {
     public static void main(String[] args) {
        
         NewJFrame ventana = new NewJFrame();
         ventana.setVisible(true);
         
        Ventana app = new Ventana();
        //app.run();
       //app.colocarEtiquetas();
       // app.colocarBotones();
      // app.colocarRadioBotones();
    // app.colocarCajasDeTexto();
    // app.ColocaropcionesHorizontales();
    //app.ColocarCheckBox();
    }
  
     JPanel panel;
    
    
    public void run(){
        //inicializacion de la ventana 
        JFrame  frm = new JFrame();
         //Panel 
        
        panel = new JPanel();
        
        panel.setLayout(null);
        
        //tamaño de la ventana
        frm.setSize(300, 500);
        
        //hacer visible la ventana 
        frm.setVisible(true);
        
        //Defecto de cierre(hace que se detenga el programa cuando se cierra la ventana: 
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //titulo de la ventana 
        frm.setTitle("VENTANA");
        
        //Localizacion de donde aparecera la ventana 
        frm.setLocation(500,200);
        
        // le ponemos el panel a la ventana
       frm.getContentPane().add(panel);
       
       //le ponemos un color al panel de la vnetana
       //panel.setBackground(Color.gray);
      
      
  }
    
    private void colocarEtiquetas(){
         //desactivacion del layaout por defecto del panel
       panel.setLayout(null);
      
      // creacion de etiqueta
      JLabel etiqueta = new JLabel();
      
      //ponerle texto a la etiqueta
      etiqueta.setText("Hola  ");
      etiqueta.setHorizontalAlignment(SwingConstants.CENTER);
     panel.add(etiqueta);
    
     //ubicacion de la etiqueta 
     etiqueta.setBounds(85,10,70,50);
     
     //color de las letraS DE la etiqueta
     etiqueta.setForeground(Color.black);
     
     //cambiar el color de fondo de la etiqueta
   //  etiqueta.setBackground(Color.black);
     
     //desactivamos la transparencia por defecto de la etiqueta
   // etiqueta.setOpaque(true);
     
     //Anulamos la fuente de texto 
    
     
     //
     Font fuente1 = new Font("Ink Free",Font.ITALIC,20);
     
     //
     etiqueta.setFont(fuente1);
     
     JLabel etiqueta2 = new JLabel();
     ImageIcon imagen = new ImageIcon("10.jpg");
     
     //posicion y lugar de la etiqueta
     etiqueta2.setBounds(10,80, 300,300);   
     panel.add(etiqueta2);
     
     //cambiar el tamaño de la imagen 
     etiqueta2.setIcon(new ImageIcon(imagen.getImage().getScaledInstance(etiqueta2.getWidth(),etiqueta2.getHeight(),Image.SCALE_SMOOTH)));
     
     
     
     
     
     
     
    }
    
    private void colocarBotones(){
        //creacion de botones
        JButton boton1 = new JButton("hola mundo");
        
        //ccreacion de fuente para el boton 
        Font fuente1 = new Font("Ink Free",0,20);
        //tamaño de botones
        boton1.setBounds(10,100, 150,40);
        //habilitar o desabilitar boton
        boton1.setEnabled(true);
        //ponerle una fuente al boton 
        boton1.setFont(fuente1);
        
        //cambiar el color a la letra del boton 
        boton1.setForeground(Color.BLUE);
        //metodo que hace que al apretar alt + una letra se seleccione el boton
        boton1.setMnemonic('a');
        //poner el boton en el panel
        panel.add(boton1);
        
        
        //-------boton con imagen
        
        JButton boton2 = new JButton();
        //tamaño y localizacion de el boton 
        boton2.setBounds(180,100, 100, 50);
        //cambio de color al boton
        //boton2.setBackground(Color.red);
        // añadimos el boton al panel 
        panel.add(boton2);
        //
        ImageIcon imagen = new ImageIcon("clic aqui1.png");
        //adaptamos la imagen a nuestro boton 
        boton2.setIcon(new ImageIcon(imagen.getImage().getScaledInstance(110,80,Image.SCALE_SMOOTH)));
    }
    
    private void colocarRadioBotones(){
        JRadioButton boton1 = new JRadioButton("opcion 1",true);
        boton1.setBounds(50,100,100, 50);
        boton1.setFont(new Font("Arial Black",0,15));
        panel.add(boton1);
        
         JRadioButton boton2= new JRadioButton("opcion 2",false);
        boton2.setBounds(50,135,100, 50);
        panel.add(boton2);
        
        JRadioButton boton3= new JRadioButton("opcion 3",false);
        boton3.setBounds(50,170,100, 50);
        panel.add(boton3);
        
        ButtonGroup Radiobotones = new ButtonGroup();
        Radiobotones.add(boton1);
        Radiobotones.add(boton2);
        Radiobotones.add(boton3);
        
        
    }
    
    private void colocarCajasDeTexto(){
        JTextField cajaTexto = new JTextField();
        cajaTexto.setBounds(100, 100, 100, 40);
        panel.add(cajaTexto);
    }
    
    private void ColocaropcionesHorizontales(){
        JComboBox sel = new JComboBox();
        sel.addItem("Azul");
        sel.addItem("Verde");
        sel.addItem("Amarillo");
        sel.setSelectedIndex(1);
        sel.setBounds(50,100, 100, 30);
        
        panel.add(sel);
    }
    
   private void ColocarCheckBox(){
        JCheckBox chA = new JCheckBox("Azul", true);
        chA.setBounds(300, 260, 100, 30);
        panel.add(chA);

        JCheckBox chB = new JCheckBox("Verde");
        chB.setBounds(420, 260, 100, 30);
        panel.add(chB);

        JCheckBox chC = new JCheckBox("Amarillo");
        chC.setBounds(540, 260, 100, 30);
        panel.add(chC);

        ButtonGroup grupo = new ButtonGroup();
        grupo.add(chA);
        grupo.add(chB);
        grupo.add(chC);

   }
}
